const { createVuePlugin } = require('vite-plugin-vue2')
import path from 'path'
module.exports = {
  plugins : [ createVuePlugin() ],
  build: {
    lib: {
      entry: path.resolve(__dirname, 'src/lib/main.js'),
      name: '@qhh/vue2-virtual-tree'
    },
    rollupOptions: {
      // 确保外部化处理那些你不想打包进库的依赖
      external: ['vue'],
      output: {
        // 在 UMD 构建模式下为这些外部化的依赖提供一个全局变量
        globals: {
          vue: 'Vue'
        }
      }
    }
  }
}
