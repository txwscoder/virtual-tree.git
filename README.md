#虚拟树


安装 
* `npm i @qhh/vue2-virtual-tree`

使用 
* `import { VirtualTree } from '@qhh/vue2-virtual-tree'`
    
* `import '@qhh/vue2-virtual-tree/dist/style.css'`

node 节点样例
```json5
{
  id: 0,
  label: "全部",
  parentId: null, // 根节点的 parentId 必须为 null
  disabled: false, // 可选
  children: [
    {
      id: 1,
      label: '全部',
      parentId: 0, // 根节点的 parentId 必须为 null
      disabled: false, // 可选
      children: null
    }
  ]
}
```

## Attributes
| 参数                 | 说明                                                                         | 类型            | 可选值                                            | 默认值          |
| ------              | :-----                                                                       | :-----         | :-----                                          | :-----        |
| itemHeight          | 每项的高度 (必填)                                                               | Number         | :-----                                          | ------        |
| options             | 树形结构默认为{labelKey: 'label',labelExtendKey: null, childrenKey: 'children', idKey: 'id', extendCountKey: null, disabledKey: 'disabled'}  labelKey: 显示的文本字段,labelExtendKey: 如果自定义的父级结构和leaf结构不一样, 则使用此值 扩展搜索, childrenKey: 子级的字段, idKey: id字段, extendCountKey: 额外的显示数量字段. 如: 需要显示在线数 节点在线字段为online: true, 则此值为'online'. 注: 此处只能显示默认为true 的字段, 如需离线数, 节点需新增字段 offline: true, disabledKey: 禁用的字段名  | Object         | :-----                                          | {childrenKey: 'children',labelKey: 'label',labelExtendKey: null, idKey: 'id', extendCountKey: null,disabledKey: 'disabled'}  |
| countOptions        | 树形结构默认为{computed: false, showCount: false, filter: false, extendCount: []}  showCount: 计算count, showCount: 显示count, filter: 数据是基于原数据还是过滤后的数据, extendCount: 默认只计算叶子节点的数量额外的显示数量字段. 如需要显示字段为online值为true, 则该值为[{key: 'online':value:true}]此处使用全等于 ,请保持数据类型一致 | Object         | :-----                                          | { showCount: false,computed: false,filter: false, extendCount: []}  |
| checkboxOptions     | 树形结构默认为{showCheckbox: false,showCheckboxLeafOnly: false,showCheckboxByKeys: null,preCheckbox: true, checkBoxSize: '12px',checkedIcon: defaultCheckedIcon,checkedPartIcon: defaultCheckedPartIcon,  checkedDisabledIcon: defaultCheckedDisabledIcon,checkedPartDisabledIcon: defaultCheckedPartDisabledIcon}  showCheckbox: 是否显示,showCheckboxLeafOnly: 是否只显示叶子节点,showCheckboxByKeys: 对象有keys值中的一个才会显示,preCheckbox: 位置前置, checkBoxSize: 大小,checkedIcon: 选中的icon,checkedPartIcon: 部分选中的icon,  checkedDisabledIcon: 禁用时选中的icon ,checkedPartDisabledIcon: 禁用时半选中的icon | Object         | :-----                                          | {showCheckbox: false,showCheckboxLeafOnly: false,showCheckboxByKeys: null,preCheckbox: true, checkBoxSize: '12px',checkedIcon: defaultCheckedIcon,checkedPartIcon: defaultCheckedPartIcon,  checkedDisabledIcon: defaultCheckedDisabledIcon,checkedPartDisabledIcon: defaultCheckedPartDisabledIcon} |
| hasInput            | 是否含有过滤输入框,输入框过滤支持多个关键词,逗号隔开(中英都可以),eg: '位置 1，位置 2,位置 3'| Boolean        | --                                              | false         |
| customSearch        | 自定义搜索块,配合searchKeyword                                                   | Boolean        | --                                              | false         |
| 配合searchKeyword    | v-model绑定值 , 用于搜索                                                        | String         | --                                              | ''            |
| placeholder         | 过滤输入框的 placeholder，配合 hasInput 使用                                      | String         | --                                              | 请输入关键字进行查找            |
| indent              | 缩进                                                                          | String, Number | --                                               | 15            |
| expandLevel         | 展开程度                                                                       | String, Number | 'all',1,2,3,,,                                   | 'all'            |
| expandKeys          | 指定 id 展开, 若指定 id 展开，则 expandLevel 失效                                  | Array          |                                                  | []           |
| expandIcon          | 自定义展开图标                                                                  | String          |                                                  |  ---          |
| foldIcon            | 自定义折叠图标                                                                  | String          |                                                  |  ---          |
| isLoading           | 是否展示'加载中...'指示状态                                                       | Boolean        | --                                               | false           |
| checkedAction       | 操作 label 执行选中                                                             | String         | 'none': 不选中；'click': 单击选中；'dblclick': 双击选中| 'none'           |
| emptyText           | 内容为空展示的文本                                                               | String         | --                                                | 暂无数据           |
| defaultCheckedKeys  | 默认选中, setData 之后赋值。                                                      | Array          | --                                               | []           |
| checkStrictly       | 在显示复选框的情况下，是否严格的遵循父子不互相关联的做法                                 | Boolean         | --                                               | false           |
| checkBoxSize        | 选择框的大小, 默认同时影响expand box的尺寸 ,除非自定义expandIcon的slot                | String          | ---                                             | 12px           |
| preCheckbox         | checkBox 是否前置(在左侧 而非右侧)                                                | Boolean          | ---                                             | true           |
| expandRotate        | 展开图标动画旋转的角度                                                            | Number          | ---                                              | 180           |
| labelClass          | 自定义label的样式                                                               | String          | ---                                              | ''           |
| itemClass           | 自定义item的样式                                                                | String          | ---                                              | ''           |
| extendFilter        | 扩展的过滤条件 以[{key1: value1, key2:value2 ...}, {...}]形式传入 会根据key和value过滤对象,数组内为并且, 对象内为或者 | Object  | ---                                            |  null(不过滤)   |
| cacheCount          | 缓存数                                                                          | Number           | ---                                            |  50            |
| throttleScrollTime  | 滚动触发的节流时间                                                                | Number           | ---                                            |  80            |
| overflowX           | 横向滚动条                                                                      | Boolean           | ---                                            |  true            |

# 方法
| 方法名称              | 说明                                 | 参数                                                       | 
| ------              | :-----                               | :-----                                                    |
|setData              | 设置tree                              | tree(Array 类型)                                           |
|getAllData           | 获取所有数据                            |--                                                         |
|setCheckedKeys       | 回显选中状态                            | id 组成的数组                                               |
|setCheckedNodes      | 回显选中状态                            | node 节点组成的数组                                          |
|setDisabledKeys      | 设置禁用状态                            | id 节点组成的数组                                          |
|setDisabledNodes      | 设置禁用状态                            | node 节点组成的数组                                          |
|getCheckedKeys       | 返回选中状态 id 组成的数组                       | --                                                         |
|getCheckedNodes      | 返回选中状态  node 节点组成的数组                 | --                                                         |
|getIndeterminateKeys | 返回半选中状态  node 节点组成的数组                 | --                                                         |
|getIndeterminateNodes| 返回半选中状态  node 节点组成的数组                 | --                                                         |
|getDisabledKeys      | 返回禁用状态  id 组成的数组                 | --                                                         |
|getDisabledNodes     | 返回禁用状态  node 节点组成的数组           | --                                                         |
|clearChecked         | 清空所有选中                            | --                                                         |
|setExpand            | 指定 id 展开                           | id 组成的数组                                                |
|showCheckedOnly      | 只展示选中的项，此方法会置空过滤条件         | isOnlyInCheckedSearch, 是否只在选中的节点里进行筛选, 默认 true   |
|restore              | 对 showCheckedOnly 之后进行恢复         | --                                                         |
|update               | 手动更新选中状态                         | --                                                         |
|clear                | 清空内存占用                            | --                                                         |

# Events
| 事件名称             | 说明                                 | 回调参数                                               | 
| ------              | :-----                              | :-----                                               |
|onChange            | 选中状态变化触发                        | （{ checkedKeys, checkedNodes })                      |
|onClickLabel        | 点击 label 触发                        | node                                                 |
|onClickCheckbox     | 点击 checkbox 触发, 获取当前点击的节点    | node                                                  |
|onClickItem         | 点击 item(不含checkbox) 触发, 获取当前点击的节点        | node                                     |
|onRightClickItem     | 点击 item 右键 触发, 获取当前点击的节点        | node                                     |


# Scoped Slot
| name      | 说明                                                                                                                           | 
| ------    | :-----                                                                                                                         |
|default    |  label 的 slot， eg： ```<template  #default="{ item }"><span><i>&#9733;</i> {{ item.label }}</span></template>```    | 
|preInput   |  input 输入框前的 slot， eg： ```<template #preInput><span>https://</span></template> ```                                         |
|loading    |  自定义加载中 slot， eg： ```<template #loading><i>加载中...</i></template> ```                                                    |
|expandIcon |  自定义展开图标 slot， eg： ```<template #expandIcon={ expand }><img v-if='expand' src="expand.jpg" /><img v-else src='flod.jpg' /></template>```                                         |
|customSearch|  自定义搜索部分 具体例子如下： |

customSearch：
```html
<!--search为搜索事件， 把keyword传入-->
<!--reset为 清空搜索事件 ,本页的keyword需自己清空!-->
<template #customSearch="{ search, reset }">
  <div class="input">
    <label>
      <input
        type="text"
        class="filter-input"
        placeholder="请输入"
        v-model="keyword"
      />
    </label>
    <i
      class="clear-input"
      v-if="keyword"
      @click="reset();keyword = '';"
    ></i>
    <button class="search-btn" @click="search(keyword)">搜索</button>
  </div>
</template>
```
gitee地址: https://gitee.com/txwscoder/virtual-tree.git
