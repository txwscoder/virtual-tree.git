import defaultCheckedIcon from "../../assets/checked-element-ui.svg";
import defaultCheckedPartIcon from "../../assets/indeterminate-element-ui.svg";
import defaultCheckedDisabledIcon from "../../assets/checked-element-ui-disabled.svg";
import defaultCheckedPartDisabledIcon from "../../assets/indeterminate-element-ui-disabled.svg";

export const defaultOptions = {
  childrenKey: 'children',
  labelKey: 'label',
  labelExtendKey: null,
  idKey: 'id',
  extendCountKey: null,
  disabledKey: 'disabled'
}
export const defaultCountOptions = {
  showCount: false,
  computed: false,
  filter: false,
  // 以{key: 'xx', value: 'xxxx'}形式的数组
  extendCount: []
}

export const defaultCheckboxOptions = {
  showCheckbox: false,
  showCheckboxLeafOnly: false,
  // 根据字段显示checkbox
  showCheckboxByKeys: null,
  // checkbox前置
  preCheckbox: true,
  // 大小
  checkBoxSize: '12px',
  // 选中的icon
  checkedIcon: defaultCheckedIcon,
  // 部分选中icon
  checkedPartIcon: defaultCheckedPartIcon,
  // 禁止时选中的icon
  checkedDisabledIcon: defaultCheckedDisabledIcon,
  // 禁止时部分选中icon
  checkedPartDisabledIcon: defaultCheckedPartDisabledIcon,
}
// checkbox 是否前置
// preCheckbox: {type: Boolean, default: true},
// // checkbox 包括expandBox 的尺寸
// checkBoxSize: { type: String, default: '12px'},
// 替换选中图标
// checkedIcon: {type: String, default: defaultCheckedIcon },
// // 替换部分选中图标
// checkedPartIcon: {type: String, default: defaultCheckedPartIcon },
// // 替换选中且禁用的图标
// checkedDisabledIcon: {type: String, default: defaultCheckedDisabledIcon },
// // 替换部分选中且禁用的图标
// checkedPartDisabledIcon: {type: String, default: defaultCheckedPartDisabledIcon },
// 是否展示checkbox
// showCheckbox: { type: Boolean, default: false },
// // showCheckboxLeafOnly
// showCheckboxLeafOnly: { type: Boolean, default: false },
