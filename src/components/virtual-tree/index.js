import VirtualTree from './index.vue'

VirtualTree.install = (app) => {
  app.component('virtual-tree', VirtualTree)
}
export default VirtualTree
