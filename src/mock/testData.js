function createMock() {
  const children = []
  for (let i = 0; i < 1; i++) {
    children.push({
      id: '1-1-2-1-1-' + i,
      deptName: '车1-1-2-1-1-' + i,
      online: false,
      isVehicle: true
    })
  }
  return children
}

export const depts = [
  {
    id: '1',
    deptName: '部门1',
    children: [
      {
        id: '1-1',
        deptName: '部门1-1',
        children: [
          {
            id: '1-1-1',
            deptName: '部门1-1-1',
            extendLabel: '测试0',
            disabledFlag: true,
            online: true,
            children: [
              {
                id: '1-1-1-1',
                deptName: '车1-1-1-1',
                online: true,
                isVehicle: true
              },
              {
                id: '1-1-1-2',
                deptName: '车1-1-1-2',
                isVehicle: true
              },
            ]
          },
          {
            id: '1-1-2',
            deptName: '部门1-1-2',
            children: [
              {
                id: '1-1-2-1',
                deptName: '部门1-1-2-1',
                children: [
                  {
                    id: '1-1-2-1-1',
                    deptName: '车1-1-2-1-1',
                    children: createMock()
                  },
                  {
                    id: '1-1-2-1-2',
                    deptName: '车1-1-2-1-2'
                  },
                  {
                    id: '1-1-2-1-3',
                    deptName: '车1-1-2-1-3',
                    online: true,
                    isVehicle: true
                  }
                ]
              },
              {
                id: '1-1-2-2',
                deptName: '部门1-1-2-2',
              },
              {
                id: '1-1-2-3',
                deptName: '车1-1-2-3',
                online: true,
                isVehicle: true
              }
            ]
          },
          {
            id: '1-1-3',
            deptName: '车1-1-3',
            online1: true
          },
          {
            id: '1-1-4',
            deptName: '部门1-1-4',
          }
        ]
      },
      {
        id: '1-2',
        deptName: '部门1-2',
      },
      {
        id: '1-3',
        deptName: '部门1-3',
        children: [
          {
            id: '1-3-1',
            deptName: '部门1-3-1',
          },
          {
            id: '1-3-2',
            deptName: '部门1-3-2',
          },
          {
            id: '1-3-3',
            deptName: '部门1-3-3',
          }
        ]
      },
      {
        id: '1-4',
        deptName: '部门1-4',
      },
    ]
  },
  {
    id: '2',
    deptName: '部门2',
  },
  {
    id: '3',
    deptName: '部门3',
    children: [
      {
        id: '3-1',
        deptName: '部门3-1',
      },
      {
        id: '3-2',
        deptName: '部门3-2',
      },
      {
        id: '3-3',
        deptName: '部门3-3',
      }
    ]
  }
]


